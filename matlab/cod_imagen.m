clc; clear all; close all;

%% filename = 'imagen.jpg';
%% filename = 'cappadocia.png';
filename = 'monks.jpg';

imagen = imread(filename);
imagen = imresize(imagen, [480 640]);

%% Ajustes hechos a la imagen de acuerdo a como son leidos por matlab
% y como van a memoria
imagen_rotada = imrotate(imagen, 90);
imagen_inv = flipud(imagen_rotada);

imagenR = imagen(:, :, 1);
imagenG = imagen(:, :, 2);
imagenB = imagen(:, :, 3);

vector = reshape(imagen_inv, [], 3);

largo = size(vector, 1);

vector_final = [];

% Debe ir escritura inversa en memoria
% del tipo
% B1
% G1
% R1
% B2 ...

vector_lr = fliplr(vector).';

fid = fopen('imagen.med','w');
fwrite(fid, vector_lr);
fid = fclose(fid);