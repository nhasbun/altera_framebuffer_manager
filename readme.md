# Altera Frambuffer Manager #

Software that works under Linux to take control of Altera Framebuffer IP Core module. I used this to project images on screen under the de10nano development platform.

In addition:

* Work with JPEG library (Independent JPEG Group) to take screenshots.
* Send JPEG file via UART (Python software as receiver).



## Notes

Pre-compiled versión under the ARM + Linux architecture is contained in *libjpeg.rar*. Extracting the RAR file make it easy to work under Embedded Command Shell  without need to do extra work to understand/verify cross-compiling.

Regardless you are going to need the JPEG library installed inside the de10nano platform. You can go to the source to find instructions and source code to accomplish this. http://www.ijg.org