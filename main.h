#include <stdio.h> // printf
#include <string.h> // strlen
#include <stdint.h> // uint8_t
#include <stdlib.h> // size_t
#include <fcntl.h> // open
#include <sys/mman.h> // mmap
#include <string.h> // strcmp
#include <unistd.h> // close
#include <time.h> // clock()
#include <setjmp.h> // usado para libjpeg

// The Altera SoC Abstraction Layer (SoCAL) API Reference Manual
#include "socal/socal.h"
#include "socal/hps.h" // para BASEADD

// Libreria de LIBJPEG
#include "./libjpeg/jpeglib.h"

// Librería Altera UART 16550
#include "alt_16550_uart.h"

// Wrapper para Altera UART 16550
#include "uart_16550_core_lib/uart_16550_core_lib.h"

// Some usefuls MACROs
#define BIT(x,n) (((x) >> (n)) & 1)
#define MASKED(x,n, mask) (((x) >> (n)) & mask)

#define BASEADD   ALT_CAST(uint32_t, ALT_LWFPGASLVS_OFST)
#define F_BUFFER  ALT_CAST(uint32_t, 0x0200)
#define UART_ADD  ALT_CAST(uint32_t, 0x0000)

#define LARGOBYTES         ALT_CAST(size_t, 4)
#define CONTROL_OFFSET     ALT_CAST(uint32_t, 0)
#define FREADER_OFFSET     ALT_CAST(uint32_t, 7*4)
#define LOCKED_MODE_OFFSET ALT_CAST(uint32_t, 9*4)
#define FRAME_ADDRESS_REG_OFFSET ALT_CAST(uint32_t, 6*4)
#define FRAME_ADDRESS      ALT_CAST(uint32_t, 0x20000000)
#define FRAME_INFO_OFFSET  ALT_CAST(uint32_t, 5*4)

#define FRAME_WIDTH  640
#define FRAME_HEIGHT 480

#define MMAPLIMIT   4096 // **
#define BYTES_FRAME 225
// ** para palabras de 32bits, se cae el programa con offset > 4096
// mmap trae programas a memoria con un limite de 4096 bytes
// leer fuera de eso puede provocar un segfault
// un frame tiene 640*480*3 bytes en pixeles, numero divisible x2 y x3

// UART PARAMETERS
#define FREQ 50000000
#define BAUDRATE 115200
#define BUFFERSIZE 10000000

void regs_report();
void start();
void stop();
void set_address();
void set_window();
void clear();
void write_fn();
void start_routine();
void write_tester();
void load_image();
void savejpeg();

// uart control functions
void uart_test();
void uart_wrapper();

// file load functions
void sendjpeg();

// bt setup
void bt_uart_config();
void bt_uart_send_com(UART * uart_p, char * com);