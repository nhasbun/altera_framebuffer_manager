#
TARGET = main

#
ALT_DEVICE_FAMILY ?= soc_cv_av

# Parameters for SCP upload. Set up SSH keys to bypass password prompt
SCP_TARGET_IP           = socfpga
SCP_USER                = root
SCP_TARGET_PATH         = /home/root/f_buffer
SCP                     = SCP
SCP_FLAGS               = -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null

HWLIBS_ROOT = $(SOCEDS_DEST_ROOT)/ip/altera/hps/altera_hps/hwlib

# Source Code
SRCS=$(wildcard *.c) $(wildcard */*.c)
OBJECTS=$(SRCS:.c=.o)

CFLAGS = -g -std=c99 -Wall -Werror -I$(HWLIBS_ROOT)/include -I$(HWLIBS_ROOT)/include/$(ALT_DEVICE_FAMILY) -D$(ALT_DEVICE_FAMILY)
LDFLAGS = -g -std=c99 -Wall -Werror -pthread -lpthread -L ./libjpeg/ -ljpeg
# CFLAGS = -O1 -std=c99 -Wall -Werror -I$(HWLIBS_ROOT)/include -I$(HWLIBS_ROOT)/include/$(ALT_DEVICE_FAMILY) -D$(ALT_DEVICE_FAMILY)
# LDFLAGS = -O1 -std=c99 -Wall -Werror -pthread -lpthread

CROSS_COMPILE = arm-linux-gnueabihf-
CC = $(CROSS_COMPILE)gcc
LD = $(CROSS_COMPILE)gcc
ARCH = arm

.PHONY: build
build: message $(TARGET) upload

$(TARGET): $(OBJECTS)
	$(LD) $(LDFLAGS) $^ -o $@

%.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(TARGET) *.a *.o *~ functions/*.o uart_16550_core_lib/*.o

# Upload to target
.PHONY: upload
upload:
	@echo "*****************************************"
	@echo "Subiendo archivo por SSH"
	@echo "*****************************************"
	$(SCP) $(SCP_FLAGS) $(TARGET) $(SCP_USER)@$(SCP_TARGET_IP):$(SCP_TARGET_PATH)
	$(SCP) $(SCP_FLAGS) complete_routine.py $(SCP_USER)@$(SCP_TARGET_IP):$(SCP_TARGET_PATH)

.PHONY: message
message:
	@echo "*****************************************"
	@echo "Localizacion SOCEDS..." $(SOCEDS_DEST_ROOT)
	@echo "*****************************************"