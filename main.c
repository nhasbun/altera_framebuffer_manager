#include "main.h"

int fd;
void * virtualbase;
uint32_t vb_int;

int main(int argc, const char * argv[])
{
  fd = open("/dev/mem", (O_RDWR|O_SYNC));
  virtualbase = mmap(NULL, LARGOBYTES,
   (PROT_READ|PROT_WRITE), MAP_SHARED, fd, BASEADD);
  vb_int = ALT_CAST(uint32_t, virtualbase);
  // MAP_SHARED comparte la memoria con otras apicaciones
  // PROT READ y PROT WRITE, para lectura y escritura

  // proceso de inicio simple
  // setadd, setwindow, start
  if(argc > 1) {
    if(strcmp(argv[1], "s") == 0)                 start_routine(); // for debug
    else if(strcmp(argv[1], "regs") == 0)         regs_report();
    else if(strcmp(argv[1], "start") == 0)        start();
    else if(strcmp(argv[1], "stop") == 0)         stop();
    else if(strcmp(argv[1], "setadd") == 0)       set_address();
    else if(strcmp(argv[1], "setwindow") == 0)    set_window();
    else if(strcmp(argv[1], "clear") == 0)        clear();
    else if(strcmp(argv[1], "write") == 0)        write_fn();
    else if(strcmp(argv[1], "writetest") == 0)    write_tester();
    else if(strcmp(argv[1], "loadimage") == 0)    load_image();
    else if(strcmp(argv[1], "savejpeg") == 0)     savejpeg();
    else if(strcmp(argv[1], "uarttest") == 0)     uart_test();
    else if(strcmp(argv[1], "uartwrapper") == 0)  uart_wrapper();
    else if(strcmp(argv[1], "sendjpeg") == 0)     sendjpeg();
    else if(strcmp(argv[1], "btuartconfig") == 0) bt_uart_config();
    else printf("%s\n", "Comando no reconocido...");
  }

  close(fd);
  return 0;
}

void regs_report()
// Deliver report from important registers signal control
{
  uint32_t control_reg =
    alt_read_word(virtualbase + F_BUFFER + CONTROL_OFFSET);
  control_reg = BIT(control_reg, 0);

  uint32_t freader_reg =
    alt_read_word(virtualbase + F_BUFFER + FREADER_OFFSET);
  freader_reg = BIT(freader_reg, 26);

  uint32_t lockedmode_reg =
    alt_read_word(virtualbase + F_BUFFER + LOCKED_MODE_OFFSET);
  lockedmode_reg = BIT(lockedmode_reg, 0);

  uint32_t startaddress_reg =
    alt_read_word(virtualbase + F_BUFFER + FRAME_ADDRESS_REG_OFFSET);

  uint32_t frame_info_reg =
    alt_read_word(virtualbase + F_BUFFER + FRAME_INFO_OFFSET);

  uint32_t mask_width = 0b1111111111111;
  uint32_t mask_height = 0b1111111111111;

  uint32_t width = MASKED(frame_info_reg, 13, mask_width);
  uint32_t height = MASKED(frame_info_reg, 0, mask_height);

  printf("%s", "Control Reg: ");
  printf("%u\n", control_reg);

  printf("%s", "Frame Reader, ready bit: ");
  printf("%u\n", freader_reg);

  printf("%s", "Locked Mode Reg: ");
  printf("%u\n", lockedmode_reg);

  printf("%s", "Start Address Reg: ");
  printf("%u\n", startaddress_reg);

  printf("%s", "Width size: ");
  printf("%u\n", width);

  printf("%s", "Height size: ");
  printf("%u\n", height);

  int page_size = sysconf(_SC_PAGE_SIZE); // **
  // averiguar el tamaño del pagefile, esta relacionado con los valores
  // que generan segfault al usar mmap
  printf("%s", "System page size: ");
  printf("%u\n", page_size);
}

void start_routine()
// Rutina simple que setea parametros basicos para el funcionamiento
// del framebuffer y video output, junto con señal de inicio
{
  set_window();
  set_address();
  start();
}

void start()
{
  printf("%s\n", "Setting control go bit...");
  alt_write_word(virtualbase + F_BUFFER + CONTROL_OFFSET, 1);
}

void stop()
{
  printf("%s\n", "Stopping control go bit...");
  alt_write_word(virtualbase + F_BUFFER + CONTROL_OFFSET, 0);
}

void set_address()
{
  printf("%s\n", "Setting start address...");
  alt_write_word(virtualbase + F_BUFFER + FRAME_ADDRESS_REG_OFFSET, FRAME_ADDRESS);
}

void set_window()
{
  printf("%s\n", "Setting window size...");
  uint32_t ancho = FRAME_WIDTH << 13;
  uint32_t alto  = FRAME_HEIGHT;
  uint32_t p_address =
    ALT_CAST(uint32_t, virtualbase) + F_BUFFER + FRAME_INFO_OFFSET;
  uint32_t frame_info_reg;
  frame_info_reg = alt_read_word(p_address);
  alt_write_word(p_address, frame_info_reg & 0);
  frame_info_reg = alt_read_word(p_address);
  alt_write_word(p_address, frame_info_reg | ancho);
  frame_info_reg = alt_read_word(p_address);
  alt_write_word(p_address, frame_info_reg | alto);
}

void clear()
// Setear pixeles en el espacio del framebuffer a color negro
{
  munmap(virtualbase, LARGOBYTES);
  close(fd);
  printf("%s\n", "Clearing frame buffer...");

  int ram_fd;
  void * v_ram;
  ram_fd = open("/dev/mem", (O_RDWR|O_SYNC));

  for(int n = 0; n < 225; n++){
    // por cada loop se limpian 4096 bytes
    // un frame contiene FRAME_WIDTH*FRAME_HEIGHT*3 bytes
    // 225 loops necesarios

    v_ram  = mmap(NULL, LARGOBYTES,
      (PROT_READ|PROT_WRITE), MAP_SHARED, ram_fd, FRAME_ADDRESS + MMAPLIMIT*n);
    uint32_t * buf = malloc(sizeof(uint32_t)*(MMAPLIMIT>>2)); // 4*1024 bytes
    memcpy(v_ram, buf, MMAPLIMIT);
    munmap(v_ram, LARGOBYTES);
  }
  close(ram_fd);
  exit(1);
}

void write_fn()
// Funcion que setea el mismo valor pixel_value a todos los pixeles en
// pantalla
{
  munmap(virtualbase, LARGOBYTES);
  close(fd);
  printf("%s\n", "Writing to frame buffer...");

  int ram_fd;
  void * v_ram;
  const int pixel_value = 200;
  ram_fd = open("/dev/mem", (O_RDWR|O_SYNC));

  for(int n = 0; n < 225; n++){
    // por cada loop se limpian 4096 bytes
    // un frame contiene FRAME_WIDTH*FRAME_HEIGHT*3 bytes
    // 225 loops necesarios

    v_ram  = mmap(NULL, LARGOBYTES,
      (PROT_READ|PROT_WRITE), MAP_SHARED, ram_fd, FRAME_ADDRESS + MMAPLIMIT*n);
    // uint32_t * buf = malloc(sizeof(uint32_t)*(MMAPLIMIT>>2)); // 4*1024 bytes
    uint8_t buf[MMAPLIMIT] = {[0 ... (MMAPLIMIT-1)] = pixel_value};
    memcpy(v_ram, buf, MMAPLIMIT);
    munmap(v_ram, LARGOBYTES);
  }
  close(ram_fd);
  exit(1);
}

void write_tester()
// Rutina que hace variar los colores de la pantalla
// Sirve para hacer exploración de funciones utilizadas y medición de tiempos
// de escritura a memoria.
{
  munmap(virtualbase, LARGOBYTES);
  close(fd);
  printf("%s\n", "Writing to frame buffer...");

  int ram_fd;
  void * v_ram;
  ram_fd = open("/dev/mem", (O_RDWR|O_SYNC));

  uint8_t pixelv = 0; // valor de pixel monocromo
  bool sumapos = true; // valores en aumento
  const uint8_t velocidad = 6;
  clock_t ti, tf;

  while(1) {
    for(int n = 0; n < 225; n++){
      // por cada loop se limpian 4096 bytes
      // un frame contiene FRAME_WIDTH*FRAME_HEIGHT*3 bytes
      // 225 loops necesarios

      v_ram  = mmap(NULL, LARGOBYTES,
        (PROT_READ|PROT_WRITE), MAP_SHARED, ram_fd, FRAME_ADDRESS + MMAPLIMIT*n);
      // uint32_t * buf = malloc(sizeof(uint32_t)*(MMAPLIMIT>>2)); // 4*1024 bytes
      uint8_t buf[MMAPLIMIT] = {[0 ... (MMAPLIMIT-1)] = pixelv};
      memcpy(v_ram, buf, MMAPLIMIT);
      munmap(v_ram, LARGOBYTES);
    }
    if(pixelv == 0){
      sumapos = true;
      ti = clock();
    }
    if(pixelv >= 256 - velocidad) {
      sumapos = false;
      tf = clock();
      double time_spent = ALT_CAST(double, tf - ti) / CLOCKS_PER_SEC;
      printf("Tiempo entre valor max y valor min %f \n", time_spent);
    }

    if(sumapos) pixelv = pixelv + velocidad;
    else        pixelv = pixelv - velocidad;
  }

  close(ram_fd);
  exit(1);
}

///////////////////////////////////////

// ** Carga de Imagen en mapa de bits **
// Imagen sin compresión en RGB con 8 bits por canal
// tamaño requerido de 640x480

void load_image() {
  FILE * f_image = fopen("imagen.med", "r");

  uint8_t * image_mem_p = malloc(sizeof(uint8_t) * FRAME_WIDTH * FRAME_HEIGHT * 3);
  fread(image_mem_p, 1, FRAME_WIDTH*FRAME_HEIGHT*3, f_image);

  munmap(virtualbase, LARGOBYTES);
  close(fd);
  printf("%s\n", "Cargando imagen de prueba...");

  int ram_fd;
  void * v_ram;
  ram_fd = open("/dev/mem", (O_RDWR|O_SYNC));

  for(int n = 0; n < 225; n++){
    // por cada loop se limpian 4096 bytes
    // un frame contiene FRAME_WIDTH*FRAME_HEIGHT*3 bytes
    // 225 loops necesarios

    v_ram  = mmap(NULL, LARGOBYTES,
      (PROT_READ|PROT_WRITE), MAP_SHARED, ram_fd, FRAME_ADDRESS + MMAPLIMIT*n);
    memcpy(v_ram, image_mem_p + MMAPLIMIT*n, MMAPLIMIT);
    munmap(v_ram, LARGOBYTES);
  }
  close(ram_fd);
  exit(1);
}

////////////////////////////////////////

// ** Guardado de Imagen en JPEG
// Imagen se guarda a archivo frame.jpg

void savejpeg()
{
  munmap(virtualbase, LARGOBYTES);
  close(fd);

  FILE * f_image = fopen("frame.jpg", "w"); // **
  // w crea archivo y si existe reescribe

  uint8_t * image_mem_p = malloc(sizeof(uint8_t) * FRAME_WIDTH * FRAME_HEIGHT * 3);

  int ram_fd;
  void * v_ram;
  ram_fd = open("/dev/mem", (O_RDWR|O_SYNC));

  for(int n = 0; n < 225; n++){
    // por cada loop se limpian 4096 bytes
    // un frame contiene FRAME_WIDTH*FRAME_HEIGHT*3 bytes
    // 225 loops necesarios

    v_ram  = mmap(NULL, LARGOBYTES,
      (PROT_READ|PROT_WRITE), MAP_SHARED, ram_fd, FRAME_ADDRESS + MMAPLIMIT*n);
    memcpy(image_mem_p + MMAPLIMIT*n, v_ram, MMAPLIMIT);
    munmap(v_ram, LARGOBYTES);
  }

  // Pixeles en memoria estan en BGR, se invierten a RGB
  for (int i = 0; i < FRAME_WIDTH * FRAME_HEIGHT; i++) {
    uint8_t R_value_temp = image_mem_p[2 + 3*i];
    uint8_t G_value_temp = image_mem_p[0 + 3*i];
    image_mem_p[0 + 3*i] = R_value_temp;
    image_mem_p[2 + 3*i] = G_value_temp;
  }

  JSAMPLE * image_buffer = (JSAMPLE*)(image_mem_p); // **
  // Points to large array of R,G,B-order data
  int image_height = FRAME_HEIGHT;
  int image_width = FRAME_WIDTH;

  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr; // **
  cinfo.err = jpeg_std_error(&jerr);
  // error manager necesario para funcionar
  // si no se hace se produce segfault
  jpeg_create_compress(&cinfo);
  jpeg_stdio_dest(&cinfo, f_image);
  cinfo.image_width = image_width;  /* image width and height, in pixels */
  cinfo.image_height = image_height;
  cinfo.input_components = 3;   /* # of color components per pixel */
  cinfo.in_color_space = JCS_RGB;
  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo, 25, TRUE /* limit to baseline-JPEG values */);
  jpeg_start_compress(&cinfo, TRUE);
  int row_stride = image_width * 3; /* JSAMPLEs per row in image_buffer */
  JSAMPROW row_pointer[1];

  while (cinfo.next_scanline < cinfo.image_height) {
    /* jpeg_write_scanlines expects an array of pointers to scanlines.
     * Here the array is only one element long, but you could pass
     * more than one scanline at a time if that's more convenient.
     */
    row_pointer[0] = & image_buffer[cinfo.next_scanline * row_stride];
    (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
  }

  jpeg_finish_compress(&cinfo);
  jpeg_destroy_compress(&cinfo);
  fclose(f_image);
  close(ram_fd);
}

void uart_test()
// Test básico de funcionamiento del uart 16550. Con esto se contruye un wrapper
// API de las funciones que andan bien en el módulo para simplificar su uso.
{
  uint32_t uart_address = (uint32_t)virtualbase + UART_ADD;
  //char * buf = "hola mundo !! \n";
  //uint8_t largo = strlen(buf) - 1;

  // ** UART OBJECT **
  ALT_16550_DEVICE_t uart_type = ALT_16550_DEVICE_ALTERA_16550_UART;
  ALT_16550_HANDLE_t uart;
  ALT_STATUS_CODE status;

  // Revisar si esta instanciado el modulo UART
  status = alt_16550_enable(&uart);
  if(status == ALT_E_SUCCESS) printf("%s\n", "ALT ENABLE OK");
  else printf("%s\n", "ERROR ESPERADO");

  // Iniciando el modulo
  status = alt_16550_init(uart_type, (void*)uart_address, FREQ, &uart);
  if(status == ALT_E_SUCCESS) printf("%s\n", "ALT INIT OK");

  // Safe reset
  status = alt_16550_reset(&uart);
  if(status == ALT_E_SUCCESS) printf("%s\n", "ALT RESET OK");

  // Set baurate and check
  status = alt_16550_baudrate_set(&uart, BAUDRATE);
  if(status == ALT_E_SUCCESS) printf("%s %i\n", "Setting BAUDRATE ", BAUDRATE);

  uint32_t baudrate = 0;
  alt_16550_baudrate_get(&uart, &baudrate);
  printf("Baudrate on register: %i\n", baudrate);

  // Enabling fifo for data
  status = alt_16550_fifo_enable(&uart);
  if(status == ALT_E_SUCCESS) printf("%s\n", "ALT FIFO ENABLE OK"); //**
    //... FIFO is automatically cleared

  // Tx idle signal
  status = alt_16550_int_enable_tx(&uart);
  if(status == ALT_E_SUCCESS) printf("%s\n", "ENABLING INTERRUPTS FOR FIFO");

  // Tx trigger event
  status = alt_16550_fifo_trigger_set_tx(&uart, ALT_16550_FIFO_TRIGGER_TX_ALMOST_EMPTY );
  if(status == ALT_E_SUCCESS) printf("%s\n", "SETTING FIFO TRIGGER ALMOST EMPTY");

  // Enable UART after config
  status = alt_16550_enable(&uart);
  if(status == ALT_E_SUCCESS) printf("%s\n", "*** ALT ENABLE OK");

  printf("%s\n", "**************************");
  printf("%s\n", "**************************");

  // Get int status
  ALT_16550_INT_STATUS_t int_status;
  status = alt_16550_int_status_get(&uart, &int_status);
  if(status == ALT_E_SUCCESS) printf("%s\n", "REQ INT_STATUS OK");
  if(int_status == ALT_16550_INT_STATUS_LINE) printf("%s\n", "line");
  if(int_status == ALT_16550_INT_STATUS_TX_IDLE) printf("%s\n", "tx_idle");

  // // Uart writing safe ** NOT WORKING
  // status = alt_16550_fifo_write_safe(&uart, "hola", 4, true);
  // if(status == ALT_E_SUCCESS) printf("%s\n", "4 bytes added to FIFO");

  // Uart writing not safe ** WORKING
  status = alt_16550_fifo_write(&uart, "hola", 4);
  if(status == ALT_E_SUCCESS) printf("%s\n", "4 bytes added to FIFO");

  // Check int status again
  // status = alt_16550_int_status_get(&uart, &int_status);
  // if(status == ALT_E_SUCCESS) printf("%s\n", "REQ INT_STATUS OK");
  // if(int_status == ALT_16550_INT_STATUS_LINE) printf("%s\n", "line");
  // if(int_status == ALT_16550_INT_STATUS_TX_IDLE) printf("%s\n", "tx_idle");

  sleep(1);
}

void uart_wrapper()
// Test de uart wrapper
{
  UART uart;
  uint32_t uart_address = (uint32_t)virtualbase + UART_ADD;
  uart_init(&uart, uart_address);
  uart_config(&uart, 115200);

  char * buf = "a";
  uart_tx(&uart, buf, 500000);
}

void sendjpeg()
// Funtion to send frame.jpg via UART
{
  FILE * f_image = fopen("frame.jpg", "r");

  fseek(f_image, 0L, SEEK_END);
  uint32_t data_size = ftell(f_image);
  rewind(f_image);

  uint8_t * frame_data = (uint8_t*)malloc(data_size * sizeof(uint8_t));
  fread(frame_data, 1, data_size, f_image);

  UART uart;
  uint32_t uart_address = (uint32_t)virtualbase + UART_ADD;
  uart_init(&uart, uart_address);
  uart_config(&uart, 115200);

  clock_t start = clock(); // tic
  uart_tx(&uart, (char*)frame_data, data_size);

  clock_t end = clock();  // toc
  float seconds = (float)(end - start) / CLOCKS_PER_SEC;

  printf("%i %s\n", data_size, "Bytes Cargados a memoria");
  printf("%s %f %s\n", "Envío por UART toma", seconds, "segundos.");
}

void bt_uart_config()
// Configuracion para hc-05 RFS Daughter Card, se debe entrar
// en modo **order-response** de forma física-manual.
{
  UART uart;
  uint32_t uart_address = (uint32_t)virtualbase + UART_ADD;
  uart_init(&uart, uart_address);
  uart_config(&uart, 38400);

  printf("\n--%s\n \n", "Configuration for HC-05 on RFS Daughter Card");

  bt_uart_send_com(&uart, "AT\r\n");
  bt_uart_send_com(&uart, "AT+VERSION?\r\n");
  bt_uart_send_com(&uart, "AT+ROLE=0\r\n");
  bt_uart_send_com(&uart, "AT+NAME=de10nano_BT\r\n");
  bt_uart_send_com(&uart, "AT+UART=115200,0,0\r\n");

  printf("\n--%s\n \n", "Configuration routine complete");

  bt_uart_send_com(&uart, "AT+RESET\r\n");
}

void bt_uart_send_com(UART * uart_p, char * com)
// Funcion para enviar comandos al modulo HC-05,
// solo para utilizarse en el order-response mode
{
  uart_tx(uart_p, com, strlen(com));
  printf("%s", com);
  sleep(1);
  for (int i = 0; i < 100; i++) uart_rx(uart_p);
  sleep(1);
  // clear_uart_fifo(uart_p);
}